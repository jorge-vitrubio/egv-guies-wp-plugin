# El Globus Vermell wp plugin

Pluggin development for the needs of the wordpress theme for [guiesbarcelona](https://guiesbarcelona.elglobusvermell.org).

Several parts of the WP will be coded here such as:
 - Widgets
 - MetaBoxes
 - etc...


---

some docs that have been used in the process

Pluggins
   - howto write a pluggin by Wordpress.org
   https://codex.wordpress.org/Writing_a_Plugin
   - and the best practices for writing pluggins
   https://developer.wordpress.org/plugins/plugin-basics/best-practices/

Widgets
   - howto write widgets
   https://www.wpbeginner.com/wp-tutorials/how-to-create-a-custom-wordpress-widget/


Meta Boxes
   - howto write a pluggin to generate MetaBoxes
  https://themefoundation.com/wordpress-meta-boxes-guide/
