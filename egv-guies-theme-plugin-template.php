<?php

/*
Plugin Name: El Globus Vermell - guies options
Plugin URI: https://elglobusvermell.org/
Description: All the <strong>options</strong> for <strong>Guies El Globus Vermell</strong> wich do not come by default with wordpress.
Author: jorge - vitrubio.net
Date: 2021 11 24
Version: 0.6
Author URI: https://vitrubio.net/
License: GPL 3.0
License URI:https://www.gnu.org/licenses/gpl-3.0.html
*/

/*
if ever read this never forget to check
howto write a pluggin by Wordpress.org
https://codex.wordpress.org/Writing_a_Plugin
and the best practices
https://developer.wordpress.org/plugins/plugin-basics/best-practices/
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );





/* * * * * * * * * * * * * * * * * * */
/*   begin pluggin for MAPSLEAFLET  */
/*
https://themefoundation.com/wordpress-meta-boxes-guide/

https://diveinwp.com/add-custom-metabox-and-save-get-meta-box-value-in-wordpress/
easy way to call metadata sotore in $foo->bar     echo esc_html( $post->bar )
https://developer.wordpress.org/reference/functions/get_post_meta/#comment-1894
*/

// - - - -
// adding metafields to category
//
// https://www.ibenic.com/custom-fields-wordpress-taxonomies/
// custom field in taxonomy
// https://metabox.io/support/topic/display-custom-field-in-taxonomy/


// Add category view blob field
// @return void
if ( ! function_exists( 'egv_category_add_blob_color' ) ) :
	function egv_category_add_blob_color($termcategoryblobcolors) {

		?>
		<div class="form-field egv-color-picker">
			<h4 for="taxImage">
				<?php _e( 'Color de los globos', 'egv-textdomain' ); ?>
			</h4>
			<p><?php _e( 'Elige el color de los globos <b>el principal</b> y <b>el fondo</b>.', 'egv-textdomain' ); ?></p>
			<script>
			jQuery(document).ready(function($){
				$('.color_field').each(function(){
					$(this).wpColorPicker();
				});
			});
			</script>
			<div class="fields">
				<label class=""><?php _e( 'color principal', 'egv-textdomain' ); ?></label>
				<input class="color_field" type="" name="_egv_category_form_blob_color" value="<?php esc_attr_e( $egv_category_blob_color ); ?>"/>
			</div>
			<div class="fields">
				<label class=""><?php _e( 'color de fondo', 'egv-textdomain' ); ?></label>
				<input class="color_field" type="" name="_egv_category_form_blob_background_color" value="<?php esc_attr_e( $egv_category_blob_background_color ); ?>"/>
			</div>
		</div>
		<?php
	}
	add_action( 'category_add_form_fields', 'egv_category_add_blob_color', 10, 2);
endif;



// Edit category blob field
// @return void
if ( ! function_exists( 'egv_category_edit_blob_color' ) ) :
	function  egv_category_edit_blob_color($term) {

		// put the term ID into a variable
		$t_id = $term->term_id;

		// retrieve the existing values for this meta field. Returns array.
		$term_egv_category_blob_color = get_term_meta( $t_id, 'egv_category_blob_color', true );
		$term_egv_category_blob_background_color = get_term_meta( $t_id, 'egv_category_blob_background_color', true );

		?>
		<script>
		jQuery(document).ready(function($){
			$('.color_field').each(function(){
				$(this).wpColorPicker();
			});
		});
		</script>
		<tr class="form-field">
			<th>
				<?php _e( 'Color de los globos', 'egv-textdomain' ); ?>
			</th>
			<td>
				<p>
					<?php _e( 'Elige el color de los globos <b>el principal</b> y <b>el fondo</b>.', 'egv-textdomain' ); ?>
				</p>
				<p>
					<label class=""><?php _e( 'color principal', 'egv-textdomain' ); ?></label>
					<input class="color_field" type="" name="_egv_category_form_blob_color" value="<?php echo esc_attr( $term_egv_category_blob_color ) ? esc_attr( $term_egv_category_blob_color ) : ''; ?>"/>
				</p>
				<p>
					<label class=""><?php _e( 'color de fondo', 'egv-textdomain' ); ?></label>
					<input class="color_field" type="" name="_egv_category_form_blob_background_color" value="<?php echo esc_attr( $term_egv_category_blob_background_color ) ? esc_attr( $term_egv_category_blob_background_color ) : ''; ?>"/>
				</p>
			</td>
		</tr>
		<?php
	}
	add_action( 'category_edit_form_fields', 'egv_category_edit_blob_color', 10 );
endif;



// Saving category blob fields
if ( ! function_exists( 'egv_category_save_blob_color' ) ) :
	function egv_category_save_blob_color( $term_id ) {
		// saves the egv_blob_color
		if ( isset ( $_POST ['_egv_category_form_blob_color' ] ) ):
			update_term_meta( $term_id, 'egv_category_blob_color', $_POST [ '_egv_category_form_blob_color' ] );
		endif;
	}
	add_action( 'edited_category', 'egv_category_save_blob_color' );
	add_action( 'create_category', 'egv_category_save_blob_color' );
endif;

if ( ! function_exists( 'egv_category_save_blob_background_color' ) ) :
	function egv_category_save_blob_background_color( $term_id ) {
		// saves the egv_blob_background_color
		if ( isset ( $_POST [ '_egv_category_form_blob_background_color' ] ) ):
			update_term_meta( $term_id, 'egv_category_blob_background_color', $_POST[ '_egv_category_form_blob_background_color' ] );
		endif;

	}
	add_action( 'edited_category', 'egv_category_save_blob_background_color' );
	add_action( 'create_category', 'egv_category_save_blob_background_color' );
endif;

// write into styles.css
// register styles and scripts
if ( ! function_exists ( 'write_category_styles_css') ) :
	function write_category_styles_css() {
		//create empty styles file
		$createstylecssfile = plugin_dir_path(__FILE__) . 'styles.css';
		file_put_contents (
				$createstylecssfile ,
				"/*
Styles for:
Plugin Name: El Globus Vermell - guies options
Plugin URI: https://guiesbarcelona.elglobusvermell.org/
Author: jorge - vitrubio.net
Authaor URI: https://vitrubio.net/
License: GPL 3.0
License URI:https://www.gnu.org/licenses/gpl-3.0.html
*/"
		);

		$categories = get_terms( array( 'taxonomy' => 'category', 'orderby' => 'name', 'order'=> 'ASC' ) );
		foreach ($categories as $eachcategory) {
			$categorytext = "\n nav";
			$categoryslug = $eachcategory->slug;
			$categoryblobcolor = get_term_meta( $eachcategory->term_id, 'egv_category_blob_color', true );
			$categoryblobbackgroundcolor = get_term_meta( $eachcategory->term_id, 'egv_category_blob_background_color', true );
			// .category-$categoryslug .egv_title h3,, nav a[href$=\"/$categoryslug/\"]
			$eachstyle = "\n .category-$categoryslug .egv_title h3{\n\t border-bottom: 1px solid $categoryblobcolor;\n}\n nav a:hover[href$=\"/$categoryslug/\"] {\n\t color: $categoryblobcolor;\n}" ;
			$stylecssfile = plugin_dir_path(__FILE__) . 'styles.css';
			file_put_contents ( $stylecssfile, $eachstyle , FILE_APPEND | LOCK_EX );
		}
	}
	// save styles whenever a category is created or modified
	add_action( 'add_category', 'write_category_styles_css' );
	add_action( 'create_category', 'write_category_styles_css' );
	add_action( 'edited_category', 'write_category_styles_css' );
	add_action( 'delete_category', 'write_category_styles_css' );
endif;

// register and enqueue styles and scripts
if ( ! function_exists ( 'egv_guies_plugin_styles') ) :
	function egv_guies_plugin_styles() {
		// wp_register_style( 'egv-guies-plugin-css', plugin_dir_url(__FILE__) . 'styles.css', array(), '', 'all'  );
		wp_enqueue_style( 'egv-guies-plugin-css', plugin_dir_url(__FILE__) . 'styles.css', array(), filemtime( plugin_dir_path(__FILE__) . 'styles.css' ), 'all'  );
	}
	add_action( 'wp_enqueue_scripts', 'egv_guies_plugin_styles');
endif;

// end adding metafields to category
// - - - -

// Adds a meta box to the post editing screen
if ( ! function_exists( 'egv_coordinates_leaflet_add_meta_box' ) ) :
	function egv_coordinates_leaflet_add_meta_box() {
		add_meta_box(
			'egv_coordinates_leaflet', //$id
			'EGV Coordinates', //$title
			'egv_coordinates_leaflet_fields', //$callback
			array ('post', 'page'), // $screen
			'side', // $context
			'high' // $priority
		);
	}
	add_action( 'add_meta_boxes', 'egv_coordinates_leaflet_add_meta_box');
endif;


// Enqueues the colorpicker scripts needed in the backend in all pages
// https://madebydenis.com/adding-metaboxes-to-your-posts-or-pages-color-picker-metabox/
// https://gist.github.com/dingo-d/a930935ad98c6f4a8a5b2db530d61be3
if ( ! is_admin() ) {
} else {
	if ( ! function_exists( 'egv_backend_scripts' ) ):
		function egv_backend_scripts( $hook ) {
			wp_enqueue_style( 'wp-color-picker');
			wp_enqueue_script( 'wp-color-picker');
		}
		add_action( 'admin_enqueue_scripts', 'egv_backend_scripts');
	endif;
};


// Outputs the content of the meta box in pages and posts
if ( ! function_exists( 'egv_coordinates_leaflet_fields' ) ) :
	function egv_coordinates_leaflet_fields( $post ) {
		wp_nonce_field( basename( __FILE__ ), 'egv_coordinates_leaflet_nonce' );
		global $post;
		?>
		<div id="egv-geolocation-options">
			<span class="egv-row-title">
				<h4><?php _e( 'Opciones de localizacion', 'egv-textdomain' ); ?></h4>
				<?php _e( 'Escribe las coordenadas <b>Long</b> y <b>Lat</b> del lugar. Puedes mirarlas en <a href="https://app.dogeo.fr/Projection/">Dogeo.fr</a> o en <a href="https://www.openstreetmap.org/">OpenStreetMaps</a>', 'egv-textdomain' ); ?>
			</span>
			<div class="row">
				<label class=""><?php _e( 'latitud', 'egv-textdomain' ); ?></label>
				<div class="fields">
					<input type="text" name="_egv_form_location_lat" value="<?php echo esc_html ($post->egv_location_lat) ?>"/>
				</div>
			</div>
			<div class="row">
				<label class=""><?php _e( 'longitud', 'egv-textdomain' ); ?></label>
				<div class="fields">
					<input type="text" name="_egv_form_location_lon" value="<?php echo esc_html ($post->egv_location_lon) ?>"/>
				</div>
			</div>
			<span class="egv-row-title">
				<h4><?php _e( 'Color de los globos', 'egv-textdomain' ); ?></h4>
				<?php _e( 'Si quieres un color especial diferente al del resto, elige el color de los globos <b>el principal</b> y <b>el fondo</b>.', 'egv-textdomain' ); ?>
			</span>
			<script>
			jQuery(document).ready(function($){
				$('.color_field').each(function(){
					$(this).wpColorPicker();
				});
			});
			</script>
			<div class="row">
				<label class=""><?php _e( 'color principal', 'egv-textdomain' ); ?></label>
				<div class="fields">
					<input class="color_field" type="" name="_egv_form_blob_color" value="<?php esc_attr_e( $post->egv_blob_color ); ?>"/>
				</div>
			</div>
			<div class="row">
				<label class=""><?php _e( 'color de fondo', 'egv-textdomain' ); ?></label>
				<div class="fields">
					<input class="color_field" type="" name="_egv_form_blob_background_color" value="<?php esc_attr_e( $post->egv_blob_background_color ); ?>"/>
				</div>
			</div>
		</div>
		<?php
	}
endif;

// Saves the custom meta input for pages and posts
if ( ! function_exists( 'egv_coordinates_leaflet_meta_save' ) ) :
	function egv_coordinates_leaflet_meta_save( $post_id ) {

		global $post;

		// Checks save status
		$is_autosave = wp_is_post_autosave( $post_id );
		$is_revision = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST[ 'egv_coordinates_leaflet_nonce' ] ) && wp_verify_nonce( $_POST[ 'egv_coordinates_leaflet_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

		// Exits script depending on save status
		if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
			return;
		}

		// Checks for input and saves if needed
		// saves the egv_map_location-lon
		if ( isset ( $_POST ['_egv_form_location_lon' ] ) ):
			update_post_meta( $post->ID, 'egv_location_lon', $_POST [ '_egv_form_location_lon' ] );
		endif;
		// saves the egv_map_location-lat
		if ( isset ( $_POST [ '_egv_form_location_lat' ] ) ):
			update_post_meta( $post->ID, 'egv_location_lat', $_POST[ '_egv_form_location_lat' ] );
		endif;
		// saves the egv_blob_color
		if ( isset ( $_POST ['_egv_form_blob_color' ] ) ):
			update_post_meta( $post->ID, 'egv_blob_color', $_POST [ '_egv_form_blob_color' ] );
		endif;
		// saves the egv_blob_background_color
		if ( isset ( $_POST [ '_egv_form_blob_background_color' ] ) ):
			update_post_meta( $post->ID, 'egv_blob_background_color', $_POST[ '_egv_form_blob_background_color' ] );
		endif;


	}
	add_action( 'save_post', 'egv_coordinates_leaflet_meta_save' );
endif;



// to include the code executed in each map loop
// if ( ! function_exists( 'egv_getdata_for_map' ) ) :
//   function egv_getdata_for_map()
//   {
//   }
// endif; //end function egv_getdata_for_map

// outputs this archive_map in front end
if ( ! function_exists( 'egv_archive_map' ) ) :
	function egv_archive_map(){
		// Detect plugin. For use on Front End only.
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		// check for plugin using plugin name
		if ( is_plugin_active( 'leaflet-map/leaflet-map.php' ) ) {
			// display the map
			//
			echo '<div class="egv_guiesmap">';

			if ( is_archive() || is_home() ) {
				// run shortcodes on php
				// https://wordpress.stackexchange.com/questions/207715/how-do-i-turn-a-shortcode-into-php-code#207721

				// display the map using CartoDB
				// https:/dd/github.com/CartoDB/basemap-styles#1-web-raster-basemaps
				echo do_shortcode( '[leaflet-map fitbounds lat="41.3922" lng="2.1755" height="100%" width="100%" tileurl=https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png  scrollwheel detect-retina]');

				// display the blobs
				// loop the post in category and get the tags
				if ( have_posts() ) :
					// Start the Loop
					while ( have_posts() ) :
						the_post();
					  // begin get data for map
				    $post = get_post( $post_id );
				    $permalink = get_the_permalink();
				    $title = get_the_title();
				    $bloblocationlat = esc_html( $post->egv_location_lat );
				    $bloblocationlon = esc_html( $post->egv_location_lon );
				    //https://yogeshchauhan.com/how-to-get-category-name-using-post-id-in-wordpress/
				    $post_categories = get_the_category( $post );
				    if ( !empty( $post_categories ) )
				    {
				      $categoryblobcolor = get_term_meta( $post_categories[0]->term_id, 'egv_category_blob_color', true );
				      $categoryblobbackgroundcolor = get_term_meta( $post_categories[0]->term_id, 'egv_category_blob_background_color', true );
				    }
				    // checks if post has color, or if category has color, else default color
				    if ( $post->egv_blob_color!=null )
				    {
				      $blobcolor = esc_html( $post->egv_blob_color );
				      if ( $post->egv_blob_background_color!=null )
				      {
				        $blobbackgroundcolor = esc_html( $post->egv_blob_background_color );
				      } else {
				        $blobbackgroundcolor = $blobcolor;
				      }
				    }
				    elseif ( $categoryblobcolor!=null )
				    {
				      $blobcolor = $categoryblobcolor;
				      if ( $categoryblobbackgroundcolor!=null ) {
				        $blobbackgroundcolor = $categoryblobbackgroundcolor;
				      } else {
				        $blobbackgroundcolor = $blobcolor;
				      }
				    } else {
				      $blobcolor = '#ff0000'; // default blob color
				      $blobbackgroundcolor = $blobcolor;
				    }//end if setting up blobcolors
				    // end getting data for map

				    // do the shortcode if ...
						$doshortcode=false;
				    if (
				        ( $bloblocationlat >= -90 && $bloblocationlat <= 90 ) &&    // lat is in valid range
				        ( $bloblocationlon >= -180 && $bloblocationlon <= 180 )  && // lon is in valid range
				        !in_category ( 'text' ) // checks that post is not text
				      ) {
				      $doshortcode=true;
				    }
						if ( $doshortcode ){
							echo do_shortcode( '[leaflet-marker svg background="' . $blobbackgroundcolor . '" iconSize="15,17" iconClass="dashicons dashicons-marker" color="' . $blobcolor . '" opacity="0.6"  lat=' . $bloblocationlat . ' lng=' . $bloblocationlon . ' ] ' . '<a href="' . $permalink . '">' . $title . '</a>'  . '[/leaflet-marker]' );
						} //end check if $bloblocation have value


					endwhile;
				endif;
			} elseif ( is_single() ) {

				// begin get data for map
				$post = get_post( $post_id );
				$permalink = get_the_permalink();
				$title = get_the_title();
				$bloblocationlat = esc_html( $post->egv_location_lat );
				$bloblocationlon = esc_html( $post->egv_location_lon );
				//https://yogeshchauhan.com/how-to-get-category-name-using-post-id-in-wordpress/
				$post_categories = get_the_category( $post );
				if ( !empty( $post_categories ) )
				{
					$categoryblobcolor = get_term_meta( $post_categories[0]->term_id, 'egv_category_blob_color', true );
					$categoryblobbackgroundcolor = get_term_meta( $post_categories[0]->term_id, 'egv_category_blob_background_color', true );
				}
				// checks if post has color, or if category has color, else default color
				if ( $post->egv_blob_color!=null )
				{
					$blobcolor = esc_html( $post->egv_blob_color );
					if ( $post->egv_blob_background_color!=null )
					{
						$blobbackgroundcolor = esc_html( $post->egv_blob_background_color );
					} else {
						$blobbackgroundcolor = $blobcolor;
					}
				}
				elseif ( $categoryblobcolor!=null )
				{
					$blobcolor = $categoryblobcolor;
					if ( $categoryblobbackgroundcolor!=null ) {
						$blobbackgroundcolor = $categoryblobbackgroundcolor;
					} else {
						$blobbackgroundcolor = $blobcolor;
					}
				} else {
					$blobcolor = '#ff0000'; // default blob color
					$blobbackgroundcolor = $blobcolor;
				}//end if setting up blobcolors
				// end getting data for map

		    // do the shortcode if ...
		    $doshortcode=false;
		    if (
		        ( $bloblocationlat >= -90 && $bloblocationlat <= 90 ) &&    // lat is in valid range
		        ( $bloblocationlon >= -180 && $bloblocationlon <= 180 )  && // lon is in valid range
		        !in_category ( 'text' ) // checks that post is not text
		      ) {
		      $doshortcode=true;
		    }	// end do the shortcode if ...

				if ( $doshortcode ){
					echo do_shortcode( '[leaflet-map zoom=17 lat=' . $bloblocationlat . ' lng=' . $bloblocationlon . ' height="100%" width="100%" tileurl=https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png  scrollwheel detect-retina]');
					echo do_shortcode( '[leaflet-marker svg background="' . $blobbackgroundcolor . '" iconClass="dashicons dashicons-marker" color="' . $blobcolor . '" opacity="0.6"  lat=' . $bloblocationlat . ' lng=' . $bloblocationlon . ' ] ' . '<a href="' . $permalink . '">' . $title . '</a>'  . '[/leaflet-marker]' );
					//echo '<div class="grid-container"style="padding-top: 2rem;"><p class="small-12"><ul><li>$blobcolor:' . $blobcolor . '</li><li>$blobbackgroundcolor:' . $blobbackgroundcolor . '</li><li>$bloblocationlat:' . $bloblocationlat . '</li><li>$bloblocationlon:' . $bloblocationlon . '</li></ul></p></div>';
				} else {
					echo do_shortcode( '[leaflet-map zoom=13 lat="41.3922" lng="2.1755" height="100%" width="100%" tileurl=https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png  scrollwheel detect-retina]');
				}
			} //end singular end check type of view
		} // end plugin check active
		else { // if no plugin pressent do other
			echo '<h5 class="">no map detected, showing category</h5>';
			echo '<div class="egv_tags">';
			wp_tag_cloud(
				array(
					'smallest'                  => 1.5,
					'largest'                   => 1.5,
					'unit'                      => 'rem', //px em rem
					'number'                    => 0,
					'format'                    => 'list', //flat  list  array
					'separator'                 => "\n\n//\n\n",
					'order'                     => 'ASC', //ASC DESC RAND
					'taxonomy'                  => 'category',
				)
			);
		}
		echo '</div>';//close the ".egv_guiesmap"
	} // end function egv_archive_map
endif;

/*   end pluggin for MAPSLEAFLET  */
/* * * * * * * * * * * * * * * * * * */
