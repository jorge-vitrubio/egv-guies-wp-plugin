<?php
/*
this is the loop to get Lat, Lon, front color and background color
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


if ( ! function_exists( 'egv_getdata_for_map' ) ) :
  function egv_getdata_for_map( $post )
  {
    // begin get data for map
    $post = get_post( $post_id );
    $permalink = get_the_permalink();
    $title = get_the_title();
    $bloblocationlat = esc_html( $post->egv_location_lat );
    $bloblocationlon = esc_html( $post->egv_location_lon );
    //https://yogeshchauhan.com/how-to-get-category-name-using-post-id-in-wordpress/
    $post_categories = get_the_category( $post );
    if ( !empty( $post_categories ) )
    {
      $categoryblobcolor = get_term_meta( $post_categories[0]->term_id, 'egv_category_blob_color', true );
      $categoryblobbackgroundcolor = get_term_meta( $post_categories[0]->term_id, 'egv_category_blob_background_color', true );
    }
    // checks if post has color, or if category has color, else default color
    if ( $post->egv_blob_color!=null )
    {
      $blobcolor = esc_html( $post->egv_blob_color );
      if ( $post->egv_blob_background_color!=null )
      {
        $blobbackgroundcolor = esc_html( $post->egv_blob_background_color );
      } else {
        $blobbackgroundcolor = $blobcolor;
      }
    }
    elseif ( $categoryblobcolor!=null )
    {
      $blobcolor = $categoryblobcolor;
      if ( $categoryblobbackgroundcolor!=null ) {
        $blobbackgroundcolor = $categoryblobbackgroundcolor;
      } else {
        $blobbackgroundcolor = $blobcolor;
      }
    } else {
      $blobcolor = '#ff0000'; // default blob color
      $blobbackgroundcolor = $blobcolor;
    }//end if setting up blobcolors
    // end getting data for map
  }
endif; //end egv_getdata_for_map

?>
